const url = "https://thoughtful-vagabond-fibre.glitch.me/coffee"

let coffees

function getCoffeeData() {
    fetch(url)
        .then(response => response.json())
        .then(json => {
            console.log(json)
            coffees = json
        })
        .catch(error => console.log(error))
}

async function getCoffeeDataAsync() {
    try {
        const response = await fetch(url)
        coffees = await response.json()
    }
    catch(error) {
        console.log(error.message);
    }
}

await getCoffeeDataAsync()

console.log("COFFEES:", coffees);