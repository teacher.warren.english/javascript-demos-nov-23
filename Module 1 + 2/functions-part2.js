// return a number
function sumTwoNumbers(num1, num2) {
    return num1 + num2
}

// arrow function 1
const sumTwoNumbersArrow = (num1, num2) => {
    return num1 + num2
}

// Single line arrow function
const sumTwoNumbersArrowSingleLine = (num1, num2) => num1 + num2


// void
function printMessage(message) {
    console.log("Message: " + message);
}

const printMessageArrow = message => console.log(message)

// arrow function with a function expression

// function expression
const squareNumber = function(number) {
    return number * number
}

// arrow function
const squareNumberArrow = (number) => number * number


// When you want to rename a function or variable, use "Rename Symbol" or F2

printMessage("Hello from the universe!")
printMessageArrow("Hello from the arrow function!")

console.log("Result of sum = " + sumTwoNumbers(5, 15))
console.log("Result of sum = " + sumTwoNumbersArrow(5, 15))
console.log("Result of sum = " + sumTwoNumbersArrowSingleLine(5, 15))

console.log(squareNumber(9));