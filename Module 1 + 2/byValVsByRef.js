// passing by value (primitives)

let myNum = 10
let mySecondNum = myNum // mySecondNum = 10

myNum += 10

console.log("myNum = ", myNum);
console.log("mySecondNum = ", mySecondNum);

// passing by ref (non-primitives)

let myObject = {
    value: 20
}

let mySecondObject = myObject // mySecondObject.value = 20

myObject.value = 50

console.log("myObject", myObject);
console.log("mySecondObject", mySecondObject);


// cannot REASSIGN the value, we can MUTATE it
// IMMUTABLE
const bird = {
    color: "Orange",
    numberOfFeet: 2,
}

// this is illegal
// bird = {
//     color: "Red",
//     numberOfFeet: 3
// }

bird.color = "Red"
bird.numberOfFeet = 3


console.log(bird);


// we can REASSIGN the value
// MUTABLE
// let 