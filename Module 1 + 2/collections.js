const myFirstCollection = [14, 64, 85, 21]

console.log(myFirstCollection);

let first = myFirstCollection[0]
let last = myFirstCollection[3]
let fifth = myFirstCollection[4]

// helper method
let lengthOfArray = myFirstCollection.length

// console.log(myFirstCollection[0]); // 14
// console.log(myFirstCollection[lengthOfArray - 1]); // 21
// console.log(myFirstCollection[lengthOfArray]); // undefined

// console.log(lengthOfArray);

const myNames = ["Warren", "Sean", "Saju"]

let numberOfElements = myNames.length

console.log(numberOfElements);

// helper functions

// PUSH

let pushValue = myNames.push("Mujahid")

console.log(myNames);
console.log(pushValue);

// POP

let popValue = myNames.pop()
console.log(myNames);
console.log(popValue);


// SLICE

const forest = ["🌲", "🌲", "🌲", "🌲"]
const forestCopy = forest.slice()

forest.push("🔥")

console.log(forest);
console.log(forestCopy);


// SPLICE

const animals = ["🐷", "🐣", "🦁"]
animals.splice(1, 2, "🐊")

console.log(animals);