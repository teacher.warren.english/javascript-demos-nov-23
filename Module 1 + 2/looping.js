const myArray = ["one", "two", "three"]

const myObject = {
    height: 50,
    weight: 50,
    hairColor: "Black",
}

myObject.height = 60

// 1. Counter (initialize)
// 2. Condition for the loop
// 3. Increment / Decrement counter

// This loop should run 10 times.
for (let i = 0; i < 10; i++) {
    console.log(i);
}

// for ... of
for (const num of myArray) {
    console.log(num);
}

// for ... in
for (const key in myObject) {
    console.log(key);
    console.log(myObject[key]);
}

console.log("END")

// while loop (pre-test)

let count = 10;

while (count >= 0) {
    console.log("Counter: ", count);
    // NB: Don't forget to manipulate your conditional variable
    count--
}

// do while loop (post-test)

do {
    // do some stuff
    console.log("Do while:", count);
    count++
} while(count < 10)