// if...else statement
function displayGradeMessage(score) {
    if (score >= 75) {
        console.log("You earned a distinction");
    }
    
    else if (score > 50) {
        console.log("You passed");
    }

    else if (score > 0) {
        console.log("You failed");
    }
}

displayGradeMessage(75)

function checkIsPositive(number) {
    // is positive
    if (number >= 0)
        return "Positive"
    
    return "Negative"
}

const result = checkIsPositive(-4)

console.log(result);

function nestedConditionals() {
    if (true) {
        console.log("Level 1");
        if (true) {
            console.log("Level 2");
            if (false) {
                console.log("This should never print");
            }
        }
    }
}

nestedConditionals()


function checkName(name) {
    switch (name) {
        case "Warren":
            console.log("Name is Warren");
            break;
        case "Sean": 
            console.log("Name is Sean");
            break;
        default:
            console.log("No matches found.");
            break;
    }
}

const names = ["Warren", "Saju", "Sean"]

for (let i = 0; i < names.length; i++)
    checkName(names[i])