// DOM ELEMENTS

const btnGetCoffees = document.getElementById("btn-get-coffees")
const coffeeDescriptionEl = document.getElementById("coffee-description")
const coffeePriceEl = document.getElementById("coffee-price")
const coffeeIdEl = document.getElementById("coffee-id")

// VARIABLES

const url = "https://thoughtful-vagabond-fibre.glitch.me/coffee"
let coffees = []

// EVENT LISTENER

btnGetCoffees.addEventListener("click", handleGetCoffees)

// EVENT HANDLER

function handleGetCoffees() {
    console.log("The button works!")
    // getCoffeeData()
    getCoffeesAsync()
}

// FUNCTIONS

// Promises (ES6)
function getCoffeeData() {
    fetch(url)
        .then(response => response.json())
        .then(json => {
            console.log(json)
            coffees = json
            renderCoffeeData()
        })
        .catch(error => console.log(error))
}

// Async & await (ES7)
async function getCoffeesAsync() {
    const response = await fetch(url)
    const json = await response.json()
    console.log(json);
    coffees = json
    renderCoffeeData()
}

function renderCoffeeData() {
    // work with
    // coffeeIdEl
    // coffeeDescriptionEl
    // coffeePriceEl

    coffeeIdEl.innerText = coffees[0].id
    coffeeDescriptionEl.innerText = coffees[0].description
    coffeePriceEl.innerText = coffees[0].price
}