
console.log(myThirdFunction())

// Declared function - HOISTED!
function myFirstFunction() {
    console.log("Hello from my function!")
}

// Function expression - NOT HOISTED
const mySecondFunction = function() {
    console.log("Hello from my function expression!")
}

let myThirdFunction = function() {
    console.log("Hello from my third function.");
}

// Function with parameters
function addTwoNumbers(num1, num2) {
    return num1 + num2
}

function doubleNumber(num) {
    return num * 2
}

const myPrintingFunction = function(message, num) {
    console.log(message + " " + num)
}

const additionResult = addTwoNumbers(5, 10)
const doubleResult = doubleNumber(7)

myPrintingFunction("My favorite number is", 99)
myPrintingFunction("My favorite number is not", 57)
myPrintingFunction("I like", 22)
myPrintingFunction("I don't like", 35)

myFirstFunction()
mySecondFunction()


console.log("First result = " + additionResult);
console.log("Double result = " + doubleResult);