function outerFunc(fName, lName) {

    const middleName = "Thomas"

    function innerFunc() {
        console.log(fName + " " + lName);
        console.log(middleName);
    }

    return innerFunc
}

const myVariable = outerFunc("Warren", "West")

myVariable()