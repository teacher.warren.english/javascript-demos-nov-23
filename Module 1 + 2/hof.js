// A function that receives another function as a parameter
// Fn (Fn())

const numbers = [45, 81, -63, 0, 5, 77, 12, -44]

// find a positive number
function isPositive(num) {
    const result = num >= 0

    return result
}

// console.log(isPositive(10));
// console.log(isPositive(0));
// console.log(isPositive(-10));

// find all of the positive numbers in the array

// FILTER

// const subset = numbers.filter(x => isPositive(x))
const positiveSubset = numbers.filter(x => x >= 0)
const negativeSubset = numbers.filter(x => x < 0)
console.log(positiveSubset);
console.log(negativeSubset);


// MAP

const collectionOfObjects = [
    {
        id: 1,
        name: "Warren",
        lastName: "West"
    },
    {
        id: 2,
        name: "Sean",
        lastName: "Skinner"
    },
    {
        id: 3,
        name: "Lily",
        lastName: "Kirchner"
    }
]

const mapResults = numbers.map(currentElement => currentElement * 2)

const secondMapResults = collectionOfObjects.map((currentObject) => {
    return `${currentObject.id} - ${currentObject.name} ${currentObject.lastName.toUpperCase()}`
})

console.log("MAP RESULTS:");
console.log(mapResults);

console.log(secondMapResults);

// REDUCE

const reduceResult = numbers.reduce((prev, current) => {
    return prev + current
}, 0)

console.log(reduceResult);