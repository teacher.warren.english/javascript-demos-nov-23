// Declaring a variable
const myConst = "I'm a constant variable."
let myLet = "I'm a let variable."
let myNumber1 = 35
let myNumber2 = 10
let myFirstBoolean = false

// Reassign values of variables
// myConst = "I've changed"
myLet = myLet + " I can be changed."
// myLet = myLet - " I can be changed."

// manipulate the numbers
myNumber1--
console.log("Result: " + myNumber1);

// modulus operator %

// ! "not" operator
// && "and" operator
// || "or" operator
// === "strict equals"
// !== "strict unequal"
// != regular unequal

console.log(myConst !== myLet)

console.log(myLet)

console.log("1" === true)

// NaN means Not a Number!

console.log(Boolean('kjadhfbkarg'));