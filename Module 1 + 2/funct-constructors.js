function MyPet(name, color, weight) {
    // properties
    this.name = name,
    this.color = color,
    this.weight = weight
}

const yoda = new MyPet("Yoda", "Black", 4.5)
const barbie = new MyPet("Barbie", "Pink", 9)

console.log(yoda);
console.log(barbie);

function myFunctionBlock() {
    const myVariable = "hello world"
    let myLetVariable
    myLetVariable = "Some value"
}

// branch in logic / conditional statement
if (myVariable) {
    console.log("TRUE");
}
else console.log("FALSE");
