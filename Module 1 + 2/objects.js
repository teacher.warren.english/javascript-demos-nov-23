

const name = "Warren"
const lastName = "West"





const myPet = {
    // properties
    name: "Yoda",
    weight: 4.5,
    color: "Black",
    type: "Cat",
    isMale: true,

    // methods
    printPetDemo() {
        console.log(this.name) // local
        console.log(name) // global
        console.log(this.lastName) // local
        console.log(lastName) // global
    },

    printPetDescription() {
        console.log("MY PET");
        console.log("======");
        console.log("NAME: ", this.name);
        console.log("WEIGHT: ", this.weight);
        console.log("COLOR: ", this.color);
        console.log("TYPE: ", this.type);
        console.log("IS-MALE: ", this.isMale);
    }
}

myPet.printPetDemo()

console.log(myPet.color);
console.log(myPet.isMale);

myPet.printPetDescription()