let balance = 0

export function withdraw(amount) {
    balance -= amount
}

export function deposit(amount) {
    balance += amount
}

export function getBalance() {
    console.log(`Bank balance: ${balance}`);
}