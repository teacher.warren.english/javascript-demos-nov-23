class Animal {
    // constructor
    constructor(name, emoji) {
        this.name = name
        this.emoji = emoji
    }

    // methods / functions
    displayInfo() {
        // ` ` -> template literals
        // " " + this.name + " " (also applies to ' ')
        console.log(`The animal's name is ${this.name} -> ${this.emoji}`)
    }

    // properties
}

class Cat extends Animal {
    #secret

    constructor(name, emoji, purrVolume) {
        super(name, emoji)
        this.purrVolume = purrVolume
    }

    // methods
    purr() {
        console.log(`${this.name} is purring at a volume of ${this.purrVolume}`)
    }

    updateSecret(secretMessage) {
        this.#secret = secretMessage
    }

    displaySecret() {
        console.log(this.#secret);
    }

    // override displayInfo()
    displayInfo() {
        // 1 = soft, 2 = medium, 3 = loud
        let purrAdjective
        switch (this.purrVolume) {
            case 1: purrAdjective = "softly"
                break
            case 2: purrAdjective = "mediumly"
                break
            case 3: purrAdjective = "loudly"
                break
            default: purrAdjective = "normally"
                break
        }
        console.log(`The cat's name is ${this.name} -> ${this.emoji} and purrs ${purrAdjective}`)
    }
}

const goldie = new Animal("Goldie", "🐶")
goldie.displayInfo()
// goldie.name
// goldie.emoji

const tom = new Cat("Tom", "😺", 3)
tom.displayInfo()
tom.purr()

tom.updateSecret("My favorite food is broccoli")
tom.displaySecret()

// console.log(tom.#secret)