// DOM ELEMENTS
const selectBox = document.getElementById("select-box")
const coffeeList = document.getElementById("coffee-list")

const coffeeDescription = document.getElementById("coffee-description")
const coffeePrice = document.getElementById("coffee-price")
const coffeeId = document.getElementById("coffee-id")

// VARIABLES
const apiUrl = "https://thoughtful-vagabond-fibre.glitch.me/coffee"
let coffees = []

// EVENT LISTENER
selectBox.addEventListener("change", handleCoffeeChange)

// EVENT HANDLER
function handleCoffeeChange(event) {
    const selectedCoffee = coffees.find(coffee => coffee.id == event.target.value) // "2" === 2 -> false, but "2" == 2 -> true

    //  console.log(selectedCoffee);

    coffeeDescription.innerText = selectedCoffee.description
    coffeePrice.innerText = selectedCoffee.price + " NOK"
    coffeeId.innerText = selectedCoffee.id
}

// FUNCTIONS
function getCoffeeData() {
    fetch(apiUrl)
        .then((response) => {
            return response.json()
        })
        .then((json) => {
            console.log(json);
            coffees = json
            renderCoffeesInList()
            populateSelectBox()
        })
        .catch((error) => {
            console.log(error.message);
        })
}

function renderCoffeesInList() {
    coffeeList.innerHTML = ''

    for (let item of coffees) {
        const newListItem = document.createElement("li")
        newListItem.innerText = item.description
        coffeeList.appendChild(newListItem)
    }
}

function populateSelectBox() {
    selectBox.innerHTML = ''

    for (let currentCoffee of coffees) {
        const newOption = document.createElement("option")
        newOption.innerText = currentCoffee.description
        newOption.value = currentCoffee.id

        selectBox.appendChild(newOption)
    }
}

// ON PAGE LOAD
getCoffeeData()