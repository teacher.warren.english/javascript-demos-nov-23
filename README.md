# Front End Development with JavaScript

Class demos for Front End course - Nov 23

## Topics

- Variables
- Functions
- Function Constructors
- JavaScript Classes
- Objects
- By Reference VS By Value
- Closures
- Collections
- Conditionals
- Looping
- Higher Order Functions (HOFs)
- Future Values
- DOM Manipulation

## Contributors

🧑‍🏫 Warren West &copy; Noroff Accelerate AS