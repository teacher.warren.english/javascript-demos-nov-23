// spread operator - array

let myNumbers = [3, 6, 9, 12]

let myBiggerArray = [-3, 0, ...myNumbers, 15, 18, 21 ]

console.log(myNumbers);
console.log(myBiggerArray);


// spread operator - objects

const myPerson = {
    name: "Warren",
    age: 31,
    favAnimal: "cat"
}

const myNewObject = {
    ...myPerson,
    favCoffee: "Latte",
    height: 178,
}

console.log(myPerson);
console.log(myNewObject);

// function arguments

function sumUpNumbers(...numbers) {
    let sum = 0;
    
    for (let item of numbers)
        sum += item

    return sum
}

console.log(sumUpNumbers(3, 4, 5, 6, 7))

function displayMessage(...strings) {
    let message = ""

    for (let item of strings)
        message += " " + item

    return message
}

console.log(displayMessage("Hello", "I", "am", "Warren"));