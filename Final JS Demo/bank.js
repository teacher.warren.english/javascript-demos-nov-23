let balance = 0 // hidden

// setters
const deposit = (amount) => {
    balance += amount
}

const withdraw = (amount) => {
    balance -= amount
}

// getter
const getBalance = () => {
    return balance
}

// object literals (ES6 feature)
const bank = {
    deposit, // deposit: deposit
    withdraw,
    getBalance,
}

export default bank