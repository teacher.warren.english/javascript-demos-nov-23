// IMPORTS
import bank from "./bank.js"

// DOM ELEMENTS
const buttonIncrease = document.getElementById("btn-increase")
const buttonDecrease = document.getElementById("btn-decrease")
const bankBalance = document.getElementById("bank-balance")
const overdraftMessage = document.getElementById("overdraft-message")

// VARIABLES
const overdraftMessageText = "You've gone into overdraft 😰"

// destructuring
let [myFavAnimal, myFavCoffee] = ["Cat", "Latte"]

// EVENT LISTENERS
buttonIncrease.addEventListener("click", handleIncrease)
buttonDecrease.addEventListener("click", handleDecrease)

// EVENT HANDLERS
function handleIncrease() {
    // increase bank balance
    bank.deposit(100)

    if (bank.getBalance() >= 0)
        overdraftMessage.innerText = ''

    renderBankBalance()
}

function handleDecrease() {
    // decrease bank balance
    bank.withdraw(30)

    if (bank.getBalance() < 0)
        overdraftMessage.innerText = overdraftMessageText

    renderBankBalance()
}

// FUNCTIONS
function renderBankBalance() {
    bankBalance.innerText = "Balance: " + bank.getBalance() + " NOK"
    updateBankBalanceCSS()
}

function updateBankBalanceCSS() {
    if (bank.getBalance() < 0) {
        // if bank balance is negative, make it red
        bankBalance.classList.add("overdraft")
        bankBalance.classList.remove("surplus")
    }
    else {
        // else make it green
        bankBalance.classList.add("surplus")
        bankBalance.classList.remove("overdraft")
    }
}

// ON PAGE LOAD
// updateBankBalanceCSS()
renderBankBalance()